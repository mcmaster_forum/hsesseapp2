/* server.js ~ start of application */

// configure our dependencies
const express = require('express');
const path = require('path');
var favicon = require('serve-favicon');
const port = process.env.PORT || 5000;
var logger = require('morgan'); // logging
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var flash = require('express-flash'); // for messaging


var session = require('express-session');
var mongoose = require('mongoose'); /* ODM for MongoDB database */
var nodemailer = require('nodemailer'); /* for sending mail, example in password reset */
var smtpTransport = require('nodemailer-smtp-transport'); /* for smtp */
var passport = require('passport');
/* for local user logins & authentication.  Do social media OAuth logins in next phase */
var LocalStrategy = require('passport-local').Strategy;
var bcrypt = require('bcrypt-nodejs'); 
/* encrypt password.  Even if a hacker gains access to our database, he cannot see the real user passwords */
var async = require('async'); /* for waterfall/ cascading events in password reset */
var crypto = require('crypto'); /* for generating random token in password reset. Crypto is now an inbuilt component of Node.js */
require('dotenv').config(); /* loads environment variables from .env file. Safer to store configuration configuration data in .ev file. Ensure to exclude file in .gitnore file */
const siteNameHSELong = "Health Systems Exchange";
const siteNameHSEShort = "HSE";
const aboutHSE = "The world's most comprehensive, free access point for evidence to support policy makers, stakeholders and researchers interested in how to strengthen or reform health systems or in how to get cost-effective programs, services and drugs to those who need them.";
const siteNameSSELong = "Social Systems Exchange";
const siteNameSSEShort = "SSE";
const aboutSSE = "The world's most comprehensive, free access point for evidence about strengthening 16 government sectors and program areas, and achieving 14 of the 17 Sustainable Development Goals.";

// ---------------------------------------->
/* Variables for testing only. Use variables in local .env file instead */
var GmailUser = 'forum.mcmaster@gmail.com';
var GmailPass = 'A10ShunOGr81';
var dbConnDev = 'mongodb://localhost/dbhseapp';
var dbConnLive = 'mongodb://systemsvanguard:a10shunogr81@ds217452.mlab.com:17452/dbhseapp';


// -------------------------------------------->
// user authentication. Via Passport.js library
//---- local authentication. (Add Scoial Authentication later)
/* by default, local strategy uses username and password, we will override with email */
passport.use(new LocalStrategy(function(username, password, done) {
  User.findOne({ username: email }, function(err, user) {
  // User.findOne({ username: username }, function(err, user) {
    if (err) return done(err);
    // if (!user) return done(null, false, { message: 'Incorrect username.' });
	if (!user) return done(null, false, { message: 'Incorrect email.' });
    user.comparePassword(password, function(err, isMatch) {
      if (isMatch) {
        return done(null, user);
      } else {
        return done(null, false, { message: 'Incorrect password.' });
      }
    });
  });
}));
//---- stay logged in while navigating site ---->
passport.serializeUser(function(user, done) {
  done(null, user.id);
});
passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});

// ------------------------------------->
// database schema 
var userSchema = new mongoose.Schema({
    username: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    resetPasswordToken: String,
    resetPasswordExpires: Date,
    userAuthLevel: { type: String, default: 'regular' },
    userAdminLevel: { type: String, default: 'regular' }
});

// -------------------------------------
// authentication & local sign in
//------ save user ------
userSchema.pre('save', function(next) {
    var user = this;
    var SALT_FACTOR = 5;

    if (!user.isModified('password')) return next();

    bcrypt.genSalt(SALT_FACTOR, function(err, salt) {
        if (err) return next(err);

        bcrypt.hash(user.password, salt, null, function(err, hash) {
            if (err) return next(err);
            user.password = hash;
            next();
        });
    });
});
//------/ on login, verify user from DB ------>
userSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

var User = mongoose.model('User', userSchema);




// ----- configure DB connection --------->
/* Variables set in .env file for better security.  .env not deployed to Git repository */
// mongoose.connect('mongodb://localhost/hsessedb');  
// mongoose.connect('mongodb://systemsvanguard:a10shunogr81@ds217452.mlab.com:17452/dbhseapp');   
mongoose.connect(dbConnDev);   

var app = express();  /* -- configure app ---*/



// ------------------------------------------>
// middleware
app.set('port', process.env.PORT || 5000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');  /* templating. Upgrade this to pug.js! */
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));    
/* ensure there is a favicon in the 'public' sub-folder */
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(session({ secret: 'session secret key' }));
app.use(passport.initialize()); /* authentication */
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));
app.use(flash());




// -------------------------------------
// routing ~ put this in a separate file later!
app.get('/', function(req, res) {
	res.render('index', { 
	  title: `${siteNameHSEShort} - ${siteNameHSELong}` , 
	  user: req.user 
	});
});

//--- Login
app.get('/signin', function(req, res) {
  res.render('signin', {
    user: req.user
  });
});

app.post('/signin', function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err) return next(err)
    if (!user) {
      return res.redirect('/signin')
    }
    req.logIn(user, function(err) {
      if (err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);
});


app.get('/login', function(req, res) {
  res.render('login', {
    user: req.user
  });
});

app.post('/login', function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err) return next(err)
    if (!user) {
      return res.redirect('/login')
    }
    req.logIn(user, function(err) {
      if (err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);
});


// --- Registration 
app.get('/signup', function(req, res) {
  res.render('signup', {
    user: req.user
  });
});

app.post('/signup', function(req, res) {
  var user = new User({
      username: req.body.username,
      email: req.body.email,
      password: req.body.password
    });
  user.save(function(err) {
    req.logIn(user, function(err) {
      res.redirect('/');
    });
  });
});

//--- Log off 
app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/login');
});




// -- get a new password 
/* Reset Password using Email Verification.  Uses waterfall aync */
app.get('/forgot', function(req, res) {
  res.render('forgot', {
    user: req.user
  });
});

app.post('/forgot', function(req, res, next) {
  async.waterfall([
    function(done) { /* generate near unique token */
      crypto.randomBytes(20, function(err, buf) {
        var token = buf.toString('hex');
        done(err, token);
      });
    },
    function(token, done) {
      User.findOne({ email: req.body.email }, function(err, user) {
        if (!user) {
          req.flash('error', 'No account with that email address exists.');
          return res.redirect('/forgot');
        }

        user.resetPasswordToken = token;
        user.resetPasswordExpires = Date.now() + 3600000; /* 1 hour expiry  */

        user.save(function(err) {
          done(err, token, user);
        });
      });
    },
    function(token, user, done) {
      var smtpTransport = nodemailer.createTransport('SMTP', {
        service: 'Gmail',
        auth: {
          user: GmailUser , 
          pass: GmailPass
        }
      });
      var mailOptions = {
        to: user.email,
        from: 'Health Systems Exchange <'+ GmailUser +'>',
        subject: 'HSE Portal Password Reset',
        text: 'You are receiving this email because you (or someone else) has requested the reset of the password for your account.\n\n' +
          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
          'http://' + req.headers.host + '/reset/' + token + '\n\n' +
          'If you did not request this, please ignore this email and your password will remain unchanged.\n'
      };
      smtpTransport.sendMail(mailOptions, function(err) {
        req.flash('info', 'An email has been sent to ' + user.email + ' with further instructions.');
        done(err, 'done');
      });
    }
  ], function(err) {
    if (err) return next(err);
    res.redirect('/forgot');
  });
});

app.get('/reset/:token', function(req, res) {
  User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
    if (!user) {
      req.flash('error', 'Password reset token is invalid or has expired.');
      return res.redirect('/forgot');
    }
    res.render('reset', {
      user: req.user
    });
  });
});

app.post('/reset/:token', function(req, res) {
  async.waterfall([
    function(done) {
      User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
        if (!user) {
          req.flash('error', 'Password reset token is invalid or has expired.');
          return res.redirect('back');
        }

        user.password = req.body.password;
        user.resetPasswordToken = undefined;
        user.resetPasswordExpires = undefined;

        user.save(function(err) {
          req.logIn(user, function(err) {
            done(err, user);
          });
        });
      });
    },
    function(user, done) {
      var smtpTransport = nodemailer.createTransport('SMTP', {
        service: 'Gmail',
        auth: {
          user: GmailUser ,
          pass: GmailPass
        }
      });
      var mailOptions = {
        to: user.email,
        from: 'Health Systems Exchange <'+ GmailUser +'>',
        subject: 'Your Password Has Been Reset',
        text: 'Hello,\n\n' +
          'This is a confirmation that the password for your account ' + user.email + ' has just been changed as requested.\n'
      };
      smtpTransport.sendMail(mailOptions, function(err) {
        req.flash('success', 'Success! Your password has been changed.');
        done(err);
      });
    }
  ], function(err) {
    res.redirect('/');
  });
});

/* -- routing continued - put this in a separate file later ! -- */
app.get('/contact', function(req, res) {
	res.render('contact', { 
	  title: `${siteNameHSEShort} - ${siteNameHSELong}` , 
	  user: req.user 
	});
});
/* send email from user contact form */
app.post('/contact', function(req, res, next) {
	let transporter = nodemailer.createTransport({
	 host: 'smtp.gmail.com',
	 port: 587,
	 secure: false, /* use SSL */
	 auth: {
			user: GmailUser ,
			pass: GmailPass
		}
	});

	var mailOptions = {
	  from: 'Health Systems Exchange <'+ GmailUser +'>',
	  to: GmailUser,
	  subject: 'Thank You for Your Email', 
	  html: '<p>Thank you for contacting us.  We will respond to your email within one business day.  Best regards.</b><br>This is still a work in progress!</b><br>Confirmation page with Thank you grapic to be shown to end user.Also, copy of email to be sent to end user.  Currently, sending the email only to us at HSE!</p>'
	};

	transporter.sendMail(mailOptions, function (err, info) {
	   if(err)
		 console.log(err)
	   else
		 console.log(info);
	});
});
//**********************

app.get('/articles', function(req, res) {
	res.render('articles', { 
	  title: `${siteNameHSEShort} - ${siteNameHSELong}` , 
	  user: req.user 
	});
});

app.get('/services', function(req, res) {
	res.render('services', { 
	  title: `${siteNameHSEShort} - ${siteNameHSELong}` , 
	  user: req.user 
	});
});

app.get('/about', function(req, res) {
	res.render('about', { 
	  title: `${siteNameHSEShort} - ${siteNameHSELong}` , 
	  user: req.user 
	});
});


app.get('/search', function(req, res) {
	res.render('search', { 
	  title: `${siteNameHSEShort} - ${siteNameHSELong}` , 
	  user: req.user 
	});
});

app.get('/pg_palindromes', function(req, res) {
	res.render('pg_palindromes', { 
	  title: `${siteNameHSEShort} - ${siteNameHSELong}` , 
	  user: req.user 
	});
});

app.get('/pg_quotes_lombardi', function(req, res) {
	res.render('pg_quotes_lombardi', { 
	  title: `${siteNameHSEShort} - ${siteNameHSELong}` , 
	  user: req.user 
	});
});

app.get('/pg_quotes_yogiberra', function(req, res) {
	res.render('pg_quotes_yogiberra', { 
	  title: `${siteNameHSEShort} - ${siteNameHSELong}` , 
	  user: req.user 
	});
});

//------/  ------













// -------------------------------------
// Start up our web application  
app.listen(port, function(err) {
    if (!err)
        console.log(`${siteNameHSEShort} - ${siteNameHSELong} web application now started, and available on port ${port} at http://localhost:${port}/ Enjoy!`);
    else console.log(err)
});